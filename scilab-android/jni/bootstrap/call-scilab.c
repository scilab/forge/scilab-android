#include <jni.h>

void Java_org_scilab_ScilabMain_sendCommand
(
  JNIEnv* env,
  jobject this,
  jstring _command
) {
  const char* command;
  jboolean is_copy;

  command = (*env)->GetStringUTFChars(env, _command, &is_copy);

  /*
   * Send the command to the Scilab's stdin.
   */

  if (is_copy == JNI_TRUE) {
    (*env)->ReleaseStringUTFChars(env, _command, command);
  }
}

jstring Java_org_scilab_ScilabMain_readResult
(
  JNIEnv* env,
  jobject this
) {
  /*
   * Read the result of a command from Scilab's stdout.
   */
  return NULL;
}
