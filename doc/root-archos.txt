+-------------------------------+
| How to root the Archos 101 G9 |
+-------------------------------+

Author: Guillaume Mazoyer <respawneral@gmail.com>

This guide *must* be followed to the letter.
If the tablet burns, kills your cat or does some nasty things, we don't take
any responsability.

This process is needed to run the minimal version of Scilab. Without a root
access, it will be impossible to install Scilab on the tablet.
This guide works for Archos 101 G9 tablet with Android version 4.0.7 made by
Archos.

!!! Before doing anything !!!
Please be sure that the tablet is charged at about 60% to avoid surprises.
Download the required files to root the tablet from the following URL.

  http://respawner.fr/files/archos_101_g9_root.tar.gz

Also, download the Android SDK to be able to connect to the device with a CLI.

Step #1
-------

  * Boot into the recovery by holding the volume dowm button and the power
    button. Once the Archos logo appears you can release the buttons.
  * Select "Update Firmware"
  * Plug the tablet into the computer with the USB cable. You should see that a
    storage device has been detected. Put the `firmware_archos_it4.aos' file
    into the device.
  * Press the power button and wait for the process to finish.

Step #2
-------

  * Power on the device and plug it into the computer.
  * Open a terminal and enter `adb devices' (assuming that `adb' is in your
    PATH) and ensure that the device is listed.
  * Go into the directory where you extract the files needed to root the
    device and send `archos.ext4.update' into the `/mnt/storage' directory of
    the tablet by running `adb push archos.ext4.update /mnt/storage'. The
    transfer will take some time (about 2 or 3 minutes).
  * Power off the device and boot into recovery like you did before.
  * In the `Archos Boot Menu' select `Recovery System'.
  * In the next menu select `Developer Edition Menu', then select
    `Remove Android Kernel' if it is present.
  * Plug the tablet into the computer and send `zImage' and 
    `initramfs.cpio.lzo'.
  * Finally, reboot the device, it should tell you that the Android has been
    updated. The tablet is now rooted.

