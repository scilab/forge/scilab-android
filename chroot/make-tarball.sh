#!/bin/bash

# Author:  Guillaume Mazoyer <respawneral@gmail.com>
# License: see Scilab license

set -e
set -o nounset

LIBS="/usr/arm-linux-gnueabi/lib"
USR="/data/local/usr"


# Copy all libs so we still have the ones we actually need.
# This could be improve to copy only libraries we need.
for f in ${LIBS}/*; do
  if [ ! "${f}" = "/usr/arm-linux-gnueabi/lib/libnss_db.so" ]; then
    cp -p -P ${f} ${USR}/lib
  fi
done

# Delete references in libraries so they do not look for toolchain libraries.
for f in ${USR}/lib/*; do
  if chrpath ${f} 2> /dev/null | grep = > /dev/null; then
    echo "Fixing library ${f}"
    chrpath -d ${f}
  fi
done

for f in ${USR}/lib/scilab/*; do
  if chrpath ${f} 2> /dev/null | grep = > /dev/null; then
    echo "Fixing library ${f}"
    chrpath -d ${f}
  fi
done

# Same as above but for binaries.
for f in ${USR}/bin/*; do
  if chrpath ${f} 2> /dev/null | grep = > /dev/null; then
    echo "Fixing binary ${f}"
    chrpath -d ${f}
  fi
done

# Remove debug symbols.
for f in $(find -type f -executable -exec sh \
  -c "file -i '{}' | grep -q 'x-executable; charset=binary'" \; -print); do
  arm-linux-gnueabi-strip --strip-debug ${f}
done

# Create script to start Scilab.
cat > /data/local/scilab.sh << EOF
#!/system/bin/sh

USR_LIB="/data/local/usr/lib"

export LD_LIBRARY_PATH="\${USR_LIB}:\${USR_LIB}/scilab:\${LD_LIBRARY_PATH}"
export HOME="/data/local/usr/tmp"
export SCI="/data/local/usr/share/scilab"

mkdir -p \${HOME}

if [ ! -f \${SCI}/modules/core/macros/lib ]; then
  echo "exec \${SCI}/modules/functions/scripts/buildmacros/buildmacros.sce" \\
    | /data/local/usr/lib/ld-linux.so.3 /data/local/usr/bin/scilab-cli-bin
fi

TMPDIR=\${HOME} /data/local/usr/lib/ld-linux.so.3 \\
  /data/local/usr/bin/scilab-cli-bin "\${@}"
EOF

# Remove useless files that take a lot of space.
rm -rf ${USR}/share/doc
rm -rf ${USR}/share/man
rm -rf ${USR}/share/info
rm -rf ${USR}/share/gtk-doc
rm -rf ${USR}/share/applications
rm -rf ${USR}/lib/*.a
# rm -rf ${USR}/lib/*.so # Fucked up some symlinks
rm -rf ${USR}/man
rm -rf ${USR}/include

# Remove useless Scilab modules, tests and help.
# No point at having them in CLI.
rm -rf ${USR}/share/scilab/modules/*/help
rm -rf ${USR}/share/scilab/modules/*/tests
rm -rf ${USR}/share/scilab/modules/xcos
rm -rf ${USR}/share/scilab/modules/umfpack
rm -rf ${USR}/share/scilab/modules/graphics
# rm -rf ${USR}/share/scilab/modules/m2sci # Cause a warning when starting if uncommented
rm -rf ${USR}/share/scilab/modules/helptools

# Keep macros scripts to build them when starting Scilab for the first time.
cp -r temp/scilab-5.4.0-beta-2/modules/functions/scripts/ \
  ${USR}/share/scilab/modules/functions

# Set the Android shell user as owner.
chown -R 10059:2000 ${USR}
chown -R 10059:2000 /data/local/scilab.sh

# Create the tarball.
cd /data/local
tar czpf /root/scilab.tar.gz scilab.sh usr

exit 0
